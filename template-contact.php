<?php /* Template Name: BaseSite Contact  */ get_header('page'); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="c-section l-margin_l">
				<div class="l-container">
					<div class="c-about__icon">
						<img src="<?php echo get_template_directory_uri(); ?>/wp-content/images/icon__mail.svg" alt="BrainUp Trieste Udine">
					</div>
					<?php the_title('<h1 class="c-contact__title">', '</h1>') ?>
					<div class="c-contact__text">
						<?php the_field('testo'); ?>
						<br><br><br>
						<a class="c-contact__icons js-hoverButton" data-tooltip="Codepen" href="https://codepen.io/jpag82/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/wp-content/images/icon__codepen.svg" alt="Codepen BrainUp"></a>
						<a class="c-contact__icons js-hoverButton" data-tooltip="Instagram" href="https://www.instagram.com/brainup.studio/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/wp-content/images/instagram-brainup.svg" alt="Instagram BrainUp"></a>
						<a class="c-contact__icons js-hoverButton" data-tooltip="info@brainupstudio.it" href="mailto:info@brainupstudio.it" ><img src="<?php echo get_template_directory_uri(); ?>/wp-content/images/mail-brainup.svg" alt="Instagram BrainUp"></a>
					</div>	
					<br><br><br>
					<div class="c-contact__form">
						<?php the_field('form'); ?>
					</div>					
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();

