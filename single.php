<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package BaseSite
 */

get_header('page'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_type() );
			// Previous/next post navigation.
			$nextimage = wp_get_attachment_image_src( get_post_thumbnail_id( get_next_post()->ID ), '
			' );
			$previmage = wp_get_attachment_image_src( get_post_thumbnail_id( get_previous_post()->ID ), '
			' );

			the_post_navigation( array(
				'prev_text' => '<span class="c-project__nav js-hoverProject" style="background-image:url(' . $previmage[0] . ')"><h2 class="c-portfolio__title txt--s__l txt--weight__bold">&larr; %title</h2></span>',			
				'next_text' =>'<span class="c-project__nav js-hoverProject" style="background-image:url(' . $nextimage[0] . ')"><h2 class="c-portfolio__title txt--s__l txt--weight__bold">%title &rarr;</h2></span>',
			) );
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
