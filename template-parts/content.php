<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BaseSite
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="c-project__header">
<div class="c-project__title">
		<?php 
		the_title( '<h1>', '</h1>' ); 
		if ( in_category('web') ) {
			echo '<div class="c-about__icon js-hoverImage"><img src="'. get_template_directory_uri() . '/wp-content/images/icon__code-big.svg" alt="BrainUp Sviluppo Web"></div>';
		} elseif ( in_category('learning') ) {
			echo '<div class="c-about__icon js-hoverImage"><img src="'. get_template_directory_uri() . '/wp-content/images/icon__learning-big.svg" alt="BrainUp Insegnamento"></div>';
		} else {
			echo '<div class="c-about__icon js-hoverImage"><img src="'. get_template_directory_uri() . '/wp-content/images/icon__design-big.svg" alt="BrainUp Creatività"></div>';
		}
		?>
</div>
	<div class="c-project__thumb js-hoverImage" style="background-image: url('<?php the_field('immagine_principale'); ?>')"></div>
	</header><!-- .entry-header -->

	<div class="entry-content l-container c-project__content">
		<?php the_content(); ?>

		<!-- basesite layout -->
		<?php
		if( get_field('layout_progetto') ):
			while ( has_sub_field('layout_progetto') ) : ?>
					<?php if( get_row_layout() == 'testo_sinistra' ): ?>
						<div class="c-project__text c-project__textL js-onView">
							<p><?php the_sub_field('txt_sinistra'); ?></p>		
						<?php if(get_sub_field('link_sx')): ?>
							<a class="c-arrowRight js-hoverButton" href="<?php the_sub_field('link_sx'); ?>" target="_blank"><span><?php the_sub_field('testo_link_sx'); ?></span><img src="<?php echo get_template_directory_uri() ?>/wp-content/images/icon__arrow-right.png" alt="BRAINUP"></a>
						<?php endif; ?>	
						</div>					
					<?php endif; ?>

					<?php if( get_row_layout() == 'testo_destra' ): ?>
						<div class="c-project__text c-project__textR js-onView">
							<p><?php the_sub_field('testo_destra'); ?></p>
						<?php if(get_sub_field('link_dx')): ?>
							<a class="c-arrowRight js-hoverButton" href="<?php the_sub_field('link_dx'); ?>" target="_blank"><span><?php the_sub_field('testo_link_dx'); ?></span><img src="<?php echo get_template_directory_uri() ?>/wp-content/images/icon__arrow-right.png" alt="BRAINUP"></a>
						<?php endif; ?>	
						</div>				
					<?php endif; ?>

					<?php if( get_row_layout() == 'immagine' ): ?>
						<img class="c-project__img js-onView js-hoverImage" src="<?php the_sub_field('immagine'); ?>" alt="Brainup web design" />
					<?php endif; ?>

					<?php if( get_row_layout() == 'immagine_w50' ): ?>
						<img class="c-project__img c-project__img--w50 js-onView js-onView--delay1 js-hoverImage" src="<?php the_sub_field('immagine_w50'); ?>" alt="Brainup web design" />
					<?php endif; ?>

					<?php if( get_row_layout() == 'video' ): ?>
						<video class="c-project__video js-onView js-hoverImage"  src="<?php the_sub_field('video'); ?>" loop autoplay></video>
					<?php endif; ?>
			<?php endwhile;
		else : endif;
		?>
		<!-- /basesite layout -->
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
