<nav id="site-navigation" class="main-navigation">
    <div class="menu-toggle js-hoverButton">
        <span class="toggle-line toggle-line--01"></span>
        <span class="toggle-line toggle-line--02"></span>
        <span class="toggle-line toggle-line--03"></span>
        <span class="toggle-line toggle-line--04"></span>
    </div>
    <?php
        wp_nav_menu( array(
            'theme_location' => 'menu-1',
            'menu_id'        => 'primary-menu',
        ) );
    ?>
</nav><!-- #site-navigation -->