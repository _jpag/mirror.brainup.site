<?php $args = array('post_type' => 'post');
$post_query = new WP_Query('category_name=creative');
if($post_query->have_posts() ) {
	while($post_query->have_posts() ) {
		$post_query->the_post();
		?>
		<!-- post thumbnail -->
		<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
			<a class="c-portfolio__item js-onView js-hoverProject" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_post_thumbnail(array(600,600)); // Declare pixel size you need inside the array ?>
				<!-- post title -->
				<h2 class="c-portfolio__title txt--s__l txt--weight__bold"><?php the_title(); ?></h2>
				<!-- /post title -->
			</a>
		<?php endif; ?>
		<!-- /post thumbnail -->
	<?php
	}
} else { 
?>
	<!-- article -->
	<article>
	<h2>Non c'è nessun progetto disponibile</h2>
	</article>
	<!-- /article -->
	<?php
}
?>