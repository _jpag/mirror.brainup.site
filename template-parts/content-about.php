<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BaseSite
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'basesite' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<h1><?php echo get_post_meta($post->ID, "BaseSite-field01", true); ?></h1><!-- .custom-field -->
	<h3><?php echo get_post_meta($post->ID, "BaseSite-field02", true); ?></h3><!-- .custom-field -->
	<p><?php echo get_post_meta($post->ID, "BaseSite-field03", true); ?></p><!-- .custom-field -->
	<img src="<?php echo get_post_meta($post->ID, "BaseSite-field04", true); ?>" alt=""><!-- .custom-field -->

</article><!-- #post-<?php the_ID(); ?> -->
