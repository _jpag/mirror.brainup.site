<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package BaseSite
 */

get_header('page'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="error-404 not-found">
				<div class="page-content">
					<div class="l-container l-margin_l">
						<h2>ERRORE 404</h2>
						<p class="fake-code">
						<span class="fake-code--red">if </span> (<span class="fake-code--blu">!</span><span class="fake-code--yellow">found</span>) {
						<div class="fake-code--div">
							<span class="fake-code--yellow">text</span>('Ops! Questa pagina non esiste. <br>
							<a href="/">Torna alla home page</a> <a href="mailto:info@brainupstudio.it">oppure scrivi per segnalare il problema.</a>')
						</div>
						}<span class="fake-code--blu"> else </span>{ <br>
						<div class="fake-code--div">
							<span class="fake-code--yellow">text</span>('Complimenti, benvenuto nella pagina che non c'è!') <br>
						</div>
						}
						</p>
					</div>
				</div>
				<!-- .page-content -->
			</section>
			<!-- .error-404 -->
		</main>
		<!-- #main -->
	</div>
	<!-- #primary -->

	<?php
get_footer();