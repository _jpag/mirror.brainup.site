<?php /* Template Name: BaseSite About  */ get_header('page'); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		
		<section class="c-section l-margin_l">
			<div class="o-verticalText">IL PROGETTO BRAINUP</div>
			<div class="l-container">
				<img src="<?php echo get_template_directory_uri(); ?>/wp-content/images/logo-small.svg" alt="BrainUp">		
				<p>
				<?php the_field('intro_text'); ?>
			</p>

				<div class="c-about__area">
					<div class="c-aboutarea__item js-onView">	
						<div class="c-about__icon js-hoverImage">
							<img src="<?php echo get_template_directory_uri(); ?>/wp-content/images/icon__design-big.svg" alt="">
						</div>
						<h2 class="txt--s__md txt--font__normal txt--col__2">
							<?php the_field('title_design'); ?>
						</h2>
						<p>
							<?php the_field('text_design'); ?>
						</p>
						<a class="o-arrowRight js-hoverButton" href="<?php the_field('link_grafica'); ?>"><?php the_field('button_design'); ?></span><img src="<?php echo get_template_directory_uri(); ?>/wp-content/images/icon__arrow-right.png" alt="BRAINUP"></a>
					</div>

					<div class="c-aboutarea__item js-onView js-onView--delay1">
						<div class="c-about__icon js-hoverImage">
							<img src="<?php echo get_template_directory_uri(); ?>/wp-content/images/icon__code-big.svg" alt="BrainUp">
						</div>
						<h2 class="txt--s__md txt--font__normal txt--col__4">
							<?php the_field('title_web'); ?>
						</h2>
						<p>
							<?php the_field('text_web'); ?>
						</p>
						<a class="o-arrowRight js-hoverButton" href="<?php the_field('link_web'); ?>"><?php the_field('button_web'); ?></span><img src="<?php echo get_template_directory_uri(); ?>/wp-content/images/icon__arrow-right.png" alt="BRAINUP"></a>
					</div>

					<div class="c-aboutarea__item js-onView js-onView--delay2">
						<div class="c-about__icon js-hoverImage">
							<img src="<?php echo get_template_directory_uri(); ?>/wp-content/images/icon__learning-big.svg" alt="BrainUp">
						</div>
						<h2 class="txt--s__md txt--font__normal txt--col__3">
							<?php the_field('title_leraning'); ?>
						</h2>
						<p>
						<?php the_field('text_learning'); ?>
						</p>
						<a class="o-arrowRight js-hoverButton" href="<?php the_field('link_learning'); ?>"><?php the_field('button_learning'); ?></span><img src="<?php echo get_template_directory_uri(); ?>/wp-content/images/icon__arrow-right.png" alt="BRAINUP"></a>
					</div>
				</div>
			</div>
		</section>

		<section class="c-about__moonlight l-container">
			<div class="c-portrait c-portrait__one js-hoverImage" style="background-image:url('<?php the_field('image-1'); ?>')">
				<svg width="100%" height="100%">
				<defs>
					<clipPath id="clipping-one">
						<rect class="c-mask__one c-mask" x="40%" y="5%" width="50%" height="30%"/>
					</clipPath>
				</defs>
				</svg>
			</div>

			<div class="c-portrait c-portrait__two js-hoverImage" style="background-image:url('<?php the_field('image-2'); ?>')">
				<svg width="100%" height="100%">
				<defs>
					<clipPath id="clipping-two">
						<rect class="c-mask__two c-mask" x="5%" y="33%" width="60%" height="25%"/>
					</clipPath>
				</defs>
				</svg>
			</div>

			<div class="c-portrait c-portrait__three js-hoverImage" style="background-image:url('<?php the_field('image-3'); ?>')">
				<svg width="100%" height="100%">
				<defs>
					<clipPath id="clipping-three">
						<rect class="c-mask__three c-mask" x="30%" y="50%" width="45%" height="30%"/>
					</clipPath>
				</defs>
				</svg>
			</div>
		</section>

		<section class="c-section">
			<div class="o-verticalText">LA MENTE</div>
			<div class="c-about__bio l-container">
				<div class="c-aboutbio__column  js-onView">
					<img src="<?php echo get_template_directory_uri() ?>/wp-content/images/luca-pagot.svg" alt="">
					<br>
					<?php the_field('text_bio'); ?>
					<br>
					<a class="o-arrowRight js-hoverButton" href="http://www.lucapagot.it" target="_blank">www.lucapagot.it</span><img src="<?php echo get_template_directory_uri(); ?>/wp-content/images/icon__arrow-right.png" alt="BRAINUP"></a>
				</div>
				<div class="o-verticalText o-verticalText--right">LE AGENZIE</div>
				
				<div class="c-aboutbio__column  js-onView  js-onView--delay1">
				<?php while(has_sub_field('job') ): ?>   
					<div class="c-aboutbio__job">
						<?php the_sub_field('job_text'); ?>
					</div>
				<?php endwhile; ?>
				</div>
			</div>
		</section>

		<section class="c-section l-margin_l">
			<div class="o-verticalText">LE ESPERIENZE</div>
			<div class="l-container c-about__experience">
				<div class="c-aboutexperience__column js-onView">
					<?php the_field('text_experience'); ?>
					<div class="c-about__experience js-onView  js-onView">
						<?php while(has_sub_field('experience') ): ?>   
							<div class="c-about__client">
								<?php the_sub_field('experience_row'); ?>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
			<div class="c-about__image js-hoverImage" style="background-image:url('<?php echo get_template_directory_uri() ?>/wp-content/images/pagot-luca-portfolio-udinese-cover.jpg')" ></div>
		</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();