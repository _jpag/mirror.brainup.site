<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BaseSite
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'BRAINUP %1$s | Pensato, disegnato e sviluppato da %2$s.', 'basesite' ), '<a href="mailto:info@brainupstudio.it">info@brainupstudio.it</a>', '<a href="http://www.lucapagot.it" target="_blank">Luca Pagot</a>' );
			?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
