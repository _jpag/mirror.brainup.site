<?php /* Template Name: BaseSite Portfolio Web  */ get_header('page'); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		
		<section class="c-section c-portfolio l-margin_l">
			<div class="o-verticalText">WEB DESIGN</div>
			<div class="l-container">
				<div class="c-about__area">
					<div class="c-aboutarea__item">	
						<div class="c-about__icon js-hoverImage">
							<img src="<?php echo get_template_directory_uri(); ?>/wp-content/images/icon__code-big.svg" alt="BrainUp Web Design">
						</div>
						<h2 class="txt--s__md txt--font__normal txt--col__4">
							<?php the_field('title'); ?>
						</h2>
						<?php the_field('text'); ?>
						<a class="c-arrowDown c-portfolio__scroll js-scroll js-hoverButton" href="#portfolio"><span>scorri</span><img src="<?php echo get_template_directory_uri() ?>/wp-content/images/icon__arrow-down.png" alt="BRAINUP"></a>
					</div>
				</div>
			</div>
		</section>

		<section id="portfolio" class="c-portfolio__loop l-container l-margin_l">
			<?php get_template_part( './template-parts/loop', 'web' ); ?>
		</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();