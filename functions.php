<?php
/**
 * BaseSite functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package BaseSite
 */

if ( ! function_exists( 'basesite_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function basesite_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on BaseSite, use a find and replace
		 * to change 'basesite' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'basesite', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'basesite' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'basesite_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'basesite_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function basesite_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'basesite_content_width', 640 );
}
add_action( 'after_setup_theme', 'basesite_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function basesite_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'basesite' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'basesite' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'basesite_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function basesite_scripts() {
	wp_enqueue_style( 'basesite-style', get_stylesheet_uri() );
		
	wp_enqueue_script( 'basesite-jquery', get_template_directory_uri() . '/js/jquery-3.2.1.min.js', array('jquery'), '3.2.1', true);	

	wp_enqueue_script( 'scrollify', get_template_directory_uri() . '/js/jquery.scrollify.js', array('jquery'), '1.0.19', true);	

	wp_enqueue_script( 'cycle', get_template_directory_uri() . '/js/jquery.cycle2.min.js', array('jquery'), '2.1.6', true);	

	wp_enqueue_script( 'cycle-swipe', get_template_directory_uri() . '/js/jquery.cycle2.swipe.min.js', array('jquery'), '2.0', true);	

	wp_enqueue_script( 'basesite-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	
	wp_enqueue_script( 'basesite-main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'basesite_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load Jetpack compatibility file.
 */
add_action( 'admin_head', 'hide_editor_home' );
function hide_editor_home() {
	$template_file = basename( get_page_template() );
	if($template_file == 'template-home.php'){ // template
		remove_post_type_support('page', 'editor');
	}
}

add_action( 'admin_head', 'hide_editor_about' );
function hide_editor_about() {
	$template_file = basename( get_page_template() );
	if($template_file == 'template-about.php'){ // template
		remove_post_type_support('page', 'editor');
	}
}

add_action( 'admin_head', 'hide_editor_contact' );
function hide_editor_contact() {
	$template_file = basename( get_page_template() );
	if($template_file == 'template-contact.php'){ // template
		remove_post_type_support('page', 'editor');
	}
}

add_action( 'admin_head', 'hide_editor_creative' );
function hide_editor_creative() {
	$template_file = basename( get_page_template() );
	if($template_file == 'template-creative.php'){ // template
		remove_post_type_support('page', 'editor');
	}
}

add_action( 'admin_head', 'hide_editor_learning' );
function hide_editor_learning() {
	$template_file = basename( get_page_template() );
	if($template_file == 'template-learning.php'){ // template
		remove_post_type_support('page', 'editor');
	}
}

add_action( 'admin_head', 'hide_editor_web' );
function hide_editor_web() {
	$template_file = basename( get_page_template() );
	if($template_file == 'template-web.php'){ // template
		remove_post_type_support('page', 'editor');
	}
}


//TGM Plugin activation

require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'basesite_register_required_plugins' );

 function basesite_register_required_plugins() {
   $plugins = array(
      array(
        'name'               => 'Contact Form 7', // The plugin name.
        'slug'               => 'contact-form-7', // The plugin slug (typically the folder name).
        'source'             => 'contact-form-7.4.9.1.zip', // The plugin source.
        'required'           => true, // this plugin is required
        'version'            => '4.9.1', // the user must use version 1.2 (or higher) of this plugin
        'force_activation'   => true, // this plugin is going to stay activated unless the user switches to another theme
	  ),
	  array(
        'name'               => 'Contact Form 7', // The plugin name.
        'slug'               => 'contact-form-7', // The plugin slug (typically the folder name).
        'source'             => 'contact-form-7.4.6.1.zip', // The plugin source.
        'required'           => true, // this plugin is required
        'version'            => '4.6.1', // the user must use version 1.2 (or higher) of this plugin
        'force_activation'   => false, // this plugin is going to stay activated unless the user switches to another theme
       ),
      array(
        'name'               => 'Advanced Custom Fields PRO', // The plugin name.
        'slug'               => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name).
        'source'             => 'advanced-custom-fields-pro.zip', // The plugin source.
        'required'           => true, // this plugin is required
        'version'            => '5.5.5', // the user must use version 1.2 (or higher) of this plugin
        'force_activation'   => false, // this plugin is going to stay activated unless the user switches to another theme
       ),
       array(
         'name'               => 'Advanced Custom Fields: Gallery Field', // The plugin name.
         'slug'               => 'acf-gallery', // The plugin slug (typically the folder name).
         'source'             => 'acf-gallery.zip', // The plugin source.
         'required'           => true, // this plugin is required
         'version'            => '1.1.1', // the user must use version 1.2 (or higher) of this plugin
         'force_activation'   => false, // this plugin is going to stay activated unless the user switches to another theme
        ),
      array(
        'name'               => 'Advanced Custom Fields: Repeater Field', // The plugin name.
        'slug'               => 'acf-repeater', // The plugin slug (typically the folder name).
        'source'             => 'acf-repeater.zip', // The plugin source.
        'required'           => true, // this plugin is required
        'version'            => '1.1.1', // the user must use version 1.2 (or higher) of this plugin
        'force_activation'   => false, // this plugin is going to stay activated unless the user switches to another theme
       ),
       array(
         'name'               => 'Advanced Custom Fields: Flexible Content Field', // The plugin name.
         'slug'               => 'acf-flexible-content', // The plugin slug (typically the folder name).
         'source'             => 'acf-flexible-content.zip', // The plugin source.
         'required'           => true, // this plugin is required
         'version'            => '1.1.1', // the user must use version 1.2 (or higher) of this plugin
         'force_activation'   => false, // this plugin is going to stay activated unless the user switches to another theme
		),
		array(
            'name'               => 'Advanced Custom Fields: Icon Font', // The plugin name.
            'slug'               => 'acf-field-icon-font-master', // The plugin slug (typically the folder name).
            'source'             => 'acf-field-icon-font-master.zip', // The plugin source.
            'required'           => true, // this plugin is required
            'version'            => '0.1', // the user must use version 1.2 (or higher) of this plugin
            'force_activation'   => false, // this plugin is going to stay activated unless the user switches to another theme
        ),

 	);

   $config = array(
     'id'           => 'basesite',                 // Unique ID for hashing notices for multiple instances of TGMPA.
     'default_path' => get_stylesheet_directory() . '/wp-content/plugins/',  // Default absolute path to bundled plugins.
     'menu'         => 'tgmpa-install-plugins', // Menu slug.
     'parent_slug'  => 'themes.php',            // Parent menu slug.
     'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
     'has_notices'  => true, // Show admin notices
     'dismissable'  => false, // the notices are NOT dismissable
     'dismiss_msg'  => 'I really, really need you to install these plugins, okay?', // this message will be output at top of nag
     'is_automatic' => true, // automatically activate plugins after installation
     'message'      => '<!--Hey there.-->', // message to output right before the plugins table
     'strings'      => array(), // The array of message strings that TGM Plugin Activation uses
   );

	tgmpa( $plugins, $config );

}