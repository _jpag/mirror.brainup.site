var isMobile = function () {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    return true;
  } else {
    return false;
  }
}

var isSmartphone = function() {
  var w = $(window).innerWidth();
  if (w <= 650) {
    return true;
  } else {
    return false;
  }
}

var BrowserDetect = {
  init: function () {
      this.browser = this.searchString(this.dataBrowser) || "Other";
      this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
  },
  searchString: function (data) {
      for (var i = 0; i < data.length; i++) {
          var dataString = data[i].string;
          this.versionSearchString = data[i].subString;

          if (dataString.indexOf(data[i].subString) !== -1) {
              return data[i].identity;
          }
      }
  },
  searchVersion: function (dataString) {
      var index = dataString.indexOf(this.versionSearchString);
      if (index === -1) {
          return;
      }

      var rv = dataString.indexOf("rv:");
      if (this.versionSearchString === "Trident" && rv !== -1) {
          return parseFloat(dataString.substring(rv + 3));
      } else {
          return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
      }
  },

    dataBrowser: [
      {string: navigator.userAgent, subString: "Edge", identity: "MS Edge"},
      {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
      {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
      {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
      {string: navigator.userAgent, subString: "Opera", identity: "Opera"},
      {string: navigator.userAgent, subString: "OPR", identity: "Opera"},

      {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"},
      {string: navigator.userAgent, subString: "Safari", identity: "Safari"}
  ]
};  

BrowserDetect.init();
if (BrowserDetect.browser == "MS Edge" || BrowserDetect.browser == "MSIE") {
  $('body').addClass('u-explorer');
} 

if (BrowserDetect.browser == "Firefox") {
  $('body').addClass('u-firefox');
} 
/*--------------- FUNCTIONS ---------------*/
var sectionH = $('.js-section').innerHeight();

var openMenu = function () {
  var t = $('#site-navigation');
  t.on('click', function () {
    $(this).toggleClass('isOpen');
  })
};

var moveElements = function () {
  var t = $('.c-hometitle_inner');
  var i = $('.c-homeimage__inner');

  function moveEl(e, el, move) {
    var x = ((e.screenX / innerWidth * 100) - 50).toFixed(0);
    var y = ((e.screenY / sectionH * 100) - 50).toFixed(0);
    var moveEl = move;

    el.css({
      'transform': 'translate(' + (x / move) + 'px, ' + (y / move) + 'px)',
      '-webkit-transform': 'translate(' + (x / move) + 'px, ' + (y / move) + 'px)',
    });

  };

  $(document).on('mousemove', function (e) {
    moveEl(e, t, 8);
    moveEl(e, i, 2);
  });
};

var svgMoves = function () {
  //varibales ------------------------------------------
  var line1 = document.getElementById("line1");
  var circle1 = document.getElementById("circle1");
  var line2 = document.getElementById("line2");
  var circle2 = document.getElementById("circle2");
  var line3 = document.getElementById("line3");
  var circle3 = document.getElementById("circle3");
  var line4 = document.getElementById("line4");
  var circle4 = document.getElementById("circle4");
  var line5 = document.getElementById("line5");
  var circle5 = document.getElementById("circle5");
  var line6 = document.getElementById("line6");
  var circle6 = document.getElementById("circle6");

  var xOrigin1 = parseInt(line1.getAttribute("x1"));
  var yOrigin1 = parseInt(line1.getAttribute("y1"));
  var xOrigin2 = parseInt(line2.getAttribute("x1"));
  var yOrigin2 = parseInt(line2.getAttribute("y1"));
  var xOrigin3 = parseInt(line3.getAttribute("x1"));
  var yOrigin3 = parseInt(line3.getAttribute("y1"));
  var xOrigin4 = parseInt(line4.getAttribute("x1"));
  var yOrigin4 = parseInt(line4.getAttribute("y1"));
  var xOrigin5 = parseInt(line5.getAttribute("x1"));
  var yOrigin5 = parseInt(line5.getAttribute("y1"));
  var xOrigin6 = parseInt(line6.getAttribute("x1"));
  var yOrigin6 = parseInt(line6.getAttribute("y1"));


  //functions ------------------------------------------
  function setInitial(line, circle) {
    var x = line.getAttribute("x1");
    var y = line.getAttribute("y1");
    circle.style.top = parseInt(y) + "%";
    circle.style.left = parseInt(x) + "%";
  }

  function lineMove(e, x1, y1, move, line, circle, xOrig, yOrig) {
    var xCord = (((x1 - 50) / move) + xOrig).toFixed(1);
    var yCord = (((y1 - 50) / move) + yOrig).toFixed(1);

    line.setAttribute("x1", xCord + "%");
    line.setAttribute("y1", yCord + "%");
    circle.style.left = xCord + "%";
    circle.style.top = yCord + "%";
  }

  //launch ------------------------------------------
  setInitial(line1, circle1);
  setInitial(line2, circle2);
  setInitial(line3, circle3);
  setInitial(line4, circle4);
  setInitial(line5, circle5);
  setInitial(line6, circle6);

  document.onmousemove = function (e) {
    var x = (e.screenX / innerWidth * 100).toFixed(3);
    var y = (e.screenY / sectionH * 100).toFixed(3);

    setTimeout(function () {
      lineMove(e, x, y, 20, line1, circle1, xOrigin1, yOrigin1);
      lineMove(e, x, y, 10, line2, circle2, xOrigin2, yOrigin2);
      lineMove(e, x, y, 20, line3, circle3, xOrigin3, yOrigin3);
      lineMove(e, x, y, 10, line4, circle4, xOrigin4, yOrigin4);
      lineMove(e, x, y, 20, line5, circle5, xOrigin5, yOrigin5);
      lineMove(e, x, y, 10, line6, circle6, xOrigin6, yOrigin6);
    }, 300);
  };
};

if ($('.home').length > 0) {
  svgMoves();
}

var scrollifyHp = function () {
  var lineL = '#learning';
  var lineW = '#web-design';
  var lineC = '#creativita';
  var lineB = '#brainup';

  var navL = '.js-learning';
  var navW = '.js-web-design';
  var navC = '.js-creativita';
  var navB = '.js-brainup';

  var resetNav = function () {
    $('.js-home__nav').removeClass('isActive');
  };

  //scroll
  $.scrollify({
    section: ".js-scrollify",
    before: function () {
      var thisS = $.scrollify.current().attr('data-section-name');
      resetNav();
      $('.js-' + thisS).addClass('isActive');
    },
  });

  //line link
  $('#circle6').on('click', function () {
    $.scrollify.move(lineW);
  });
  $('#circle5').on('click', function () {
    $.scrollify.move(lineC);
  });

  $('#circle4').on('click', function () {
    $.scrollify.move(lineC);
  });
  $('#circle3').on('click', function () {
    $.scrollify.move(lineL);
  });

  $('#circle2').on('click', function () {
    $.scrollify.move(lineW);
  });
  $('#circle1').on('click', function () {
    $.scrollify.move(lineL);
  });

  //scroll up
  $('.c-home__arrowUp').on('click', function () {
    $.scrollify.previous();
  });

  //nav link
  $('.js-creativita').on('click', function () {
    $.scrollify.move(lineC);
  });
  $('.js-web-design').on('click', function () {
    $.scrollify.move(lineW);
  });
  $('.js-learning').on('click', function () {
    $.scrollify.move(lineL);
  });
  $('.js-brainup').on('click', function () {
    $.scrollify.move(lineB);
  });
};

var cycleHome = function() {
  if (isSmartphone() === true) {
    $( '.js-cycle' ).cycle({
      slides: '> .item',
      fx: 'scrollHorz',
      swipe: true,
    });

  }
  
};

var preloader = function () {
  if ($('.home').length > 0) {

    var interval = setInterval(function () {
      if (document.readyState === 'complete') {
        clearInterval(interval);
        $('#preloader').delay(4400).fadeOut(); // will first fade out the loading animation 
        $('.preloader-logo').delay(3800).fadeOut('slow'); // will fade out the white DIV that covers the website. 
        $('body').delay(4400).css({
          'overflow-x': 'visible'
        });
      }
    }, 100);
  } else {
    $('.page-brainup').delay(200).fadeIn('slow');
  }
};

var moonlightFx = function () {
  //--------------------
  //global variables
  //--------------------
  var maskOne = $('.c-mask__one');
  var maskTwo = $('.c-mask__two');
  var maskThree = $('.c-mask__three');
  var section = $('.c-about__moonlight');

  //--------------------
  //global functions
  //--------------------
  function moveMask(e,maskEl, operY, operX) {
    //var e = window.event;
    var winX = $(section).innerWidth() / 2;
    var winY = $(section).innerHeight() / 2;
    var posX = ((e.screenX - winX) / operX).toFixed(0);
    var posY = ((e.screenY - winY) / operY).toFixed(0);
    maskEl.css({
      transform: 'translate(' + posX + 'px, ' + posY + 'px)',
      '-webkit-transform': 'translate(' + posX + 'px, ' + posY + 'px)',
      '-moz-transform': 'translate(' + posX + 'px, ' + posY + 'px)',
      '-ms-transform': 'translate(' + posX + 'px, ' + posY + 'px)',
    });
  };

  function resetPos(maskEl) {
    maskEl.css({
      transform: 'translate(0, 0)',
      '-webkit-transform': 'translate(0, 0)',
      '-moz-transform': 'translate(0, 0)',
      '-ms-transform': 'translate(0, 0)',
    });
  };



  //--------------------
  //launch functions
  //--------------------

  $(document,window,'html').on('mousemove',function (e) {
    setTimeout(
      moveMask(e,maskOne, 10, 15), 1600);
  }).mouseleave(function () {
    resetPos(maskOne);
  });

  $(document,window,'html').on('mousemove',function (e) {
    setTimeout(
      moveMask(e,maskTwo, 10, 20), 2800);
  }).mouseleave(function () {
    resetPos(maskTwo);
  });

  $(document,window,'html').on('mousemove',function (e) {
    setTimeout(
      moveMask(e,maskThree, 5, 10), 500);
  }).mouseleave(function () {
    resetPos(maskThree);
  });

};

var onView  = function() {
  var el = $('.js-onView');
  var win = $(window).innerHeight() / 1.3;

  $(window).scroll(function (event) {
    var scroll = $(window).scrollTop() + win;

    el.each(function() {
      var thisEl = +($(this).offset().top);
      var endEl = +(thisEl + $(this).innerHeight());
      if (scroll > thisEl && scroll < endEl) {
        $(this).addClass('isVisible');
      }
    });
  });
  if ($('.page-brainup').length > 0) {
      $('html, body').animate({ scrollTop: "2px" });
  }
};

var isScrolled = function() {
  $(window).scroll(function (event) {
      var scroll = $(window).scrollTop();
      $('body').toggleClass("isScrolled", scroll > 100);
  });
};

var customSelect = function() {

  $('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden'); 
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

  });
}

var anchorScroll = function() {
  $('.js-scroll').click(function(){
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top - 120
        }, 500);
        return false;
    });
}


var customCursor = function() {
  var cursorId = $('#cursor');

  function cursorFn(event, elCursor) {
      var posX = event.pageX -15;
      var posY = event.pageY -15;

      elCursor.css({
        transform: 'translate('+posX+'px, '+posY+'px)',
        '-webkit-transform': 'translate('+posX+'px, '+posY+'px)',
        '-moz-transform': 'translate('+posX+'px, '+posY+'px)',
        '-ms-transform': 'translate('+posX+'px, '+posY+'px)',
      });
  };
  $(document,window,'html').mouseleave(function(event){
    cursorId.removeClass('isVisible');
    cursorId.removeClass('isHover');
  }).mouseenter(function(){

    $(this).on('mousemove', function(event){
      cursorFn(event, cursorId);
      cursorId.addClass('isVisible');

    });
  });

  function isMouseHover(elCursor, elHover) {
    $('.js-hover' + elHover).mouseover(function() {
      elCursor.addClass('isHover' + elHover);
    });
    $('.js-hover' + elHover).mouseout(function() {
      elCursor.removeClass('isHover' + elHover);
    });
  }
  isMouseHover(cursorId, 'Button');
  isMouseHover(cursorId, 'Image');
  isMouseHover(cursorId, 'Project');

};


/*--------------- LOAD FUNCTIONS ---------------*/
preloader();
onView();

$(document).ready(function () {
  openMenu();
  moonlightFx();
  isScrolled();
  customSelect();
  cycleHome();
  anchorScroll();
  customCursor();

  if ($('.home').length > 0) {
    if (isSmartphone() === false) {
      moveElements();
    }
    scrollifyHp();
    $.scrollify.instantMove('#creativita');
  }

});

