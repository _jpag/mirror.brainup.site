<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BaseSite
 */

get_header(); ?>
	<div id="preloader">
		<div class="preloader-logo">
			<svg width="196px" height="116px" viewBox="0 0 196 116" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<defs>
				<path d="M38.5729157,0 L6.70081757,9.75228581 L0,39.1028027 L23.2664452,64.3022008 L34.7124042,61.9874754 L39.0660715,52.1616 L55.1775213,51.3504427 L75.6155233,48.1877652 L89.3255927,36.5054266 L92.9081744,22.9582623 L75.4748638,7.84732036 L38.5729157,0 Z M19.5008366,24.0754852 L9.39368533,11.0267227 L37.6510025,3.635992 L53.6336557,31.0330412 L53.60993,31.0547836 L19.5008366,24.0754852 Z M41.296288,3.90191782 L73.9445557,10.8444214 L73.9970912,10.8862336 L54.3860995,30.2871109 L41.296288,3.90191782 Z M64.2644672,22.6421618 L74.6309065,11.3662381 L90.6474535,23.5018213 L86.3988572,34.7075005 L74.9173097,44.4915636 L64.2644672,22.6421618 Z M3.61647564,38.020702 L8.4819403,11.9917491 L33.8057433,47.5689441 L3.61647564,38.020702 Z M55.3842739,32.2991157 L61.9918834,25.1124286 L74.1716446,45.1237646 L54.925012,48.1108307 L40.1659275,48.8517435 L55.3554641,32.330893 L55.4046102,32.3392554 L55.3842739,32.2991157 Z M22.2665761,27.6462501 L52.7015743,31.9545829 L37.3595153,47.1340968 L22.2665761,27.6462501 Z M5.12983682,40.0126369 L35.7648088,51.5009667 L32.3805062,59.1442434 L23.7951896,62.3922178 L5.12983682,40.0126369 Z" id="path-1"></path>
				<linearGradient x1="100%" y1="0%" x2="0%" y2="100%" id="linearGradient-3">
					<stop stop-color="#E00146" offset="0%"></stop>
					<stop stop-color="#2B0E82" offset="100%"></stop>
				</linearGradient>
			</defs>
			<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<g id="load__03" transform="translate(-622.000000, -467.000000)">
					<g id="Group" transform="translate(622.000000, 467.000000)">
						<g id="logo" transform="translate(52.000000, 0.000000)">
							<mask id="mask-2" fill="white">
								<use xlink:href="#path-1"></use>
							</mask>
							<rect id="gradient" fill="url(#linearGradient-3)" mask="url(#mask-2)" x="-3.61538462" y="-1.30220075" width="96.4102564" height="67"></rect>
						</g>
						<g id="logo_name--white" transform="translate(0.000000, 79.014493)" fill="#FFFFFF">
							<g id="logo-name" transform="translate(0.000000, 1.681818)">
								<path id="letterB" d="M13.3727641,32.0872409 C15.1659692,32.0872409 16.7032427,31.4464682 17.9862684,30.1649227 C19.2524564,28.8665591 19.8788154,27.32265 19.8619778,25.5315136 C19.8619778,23.7403773 19.220465,22.2048773 17.9374393,20.9233318 C16.6544137,19.6586045 15.1171402,19.0262409 13.323935,19.0262409 L2.66404615,19.0262409 L2.66404615,32.0872409 L13.3727641,32.0872409 Z M2.66404615,3.32646818 L2.66404615,16.3639227 L12.5089949,16.3639227 C14.3190376,16.3471045 15.8478923,15.7063318 17.0989265,14.4416045 C18.3651145,13.1600591 18.9982085,11.6245591 18.9982085,9.83342273 C18.9813709,8.04228636 18.3398581,6.50678636 17.0736701,5.22524091 C15.7906444,3.95883182 14.2533709,3.32646818 12.4601658,3.32646818 L2.66404615,3.32646818 Z M19.8367214,19.0262409 C21.6299265,20.8005591 22.5256872,22.9600136 22.5256872,25.5079682 C22.5425248,28.0542409 21.6551829,30.2221045 19.8619778,32.0132409 C18.0856103,33.8211955 15.7249778,34.7327409 12.7800803,34.7495591 L12.7800803,34.7731045 L0.000336752137,34.7731045 L0.000336752137,0.665831818 L13.1252513,0.665831818 C15.2316359,0.680968182 17.173012,1.58578636 18.9493795,3.37692273 C20.7425846,5.13442273 21.6467641,7.27874091 21.6636017,9.80819545 C21.6787556,12.3393318 20.7914137,14.4987864 18.9982085,16.2899227 C18.8012085,16.5035136 18.3819521,16.8819227 17.7404393,17.4234682 C18.678294,18.0810591 19.3770547,18.6141955 19.8367214,19.0262409 Z" id="Fill-1"></path>
								<path id="letterR" d="M31.8217299,3.32697273 L31.8217299,21.6133818 L40.974653,21.6133818 C43.5087128,21.6133818 45.6723453,20.7253818 47.4638667,18.9510636 C49.2402342,17.1447909 50.1292598,14.9836545 50.1292598,12.4693364 C50.1292598,9.9567 49.2402342,7.80397273 47.4638667,6.01283636 C45.6723453,4.2217 43.5087128,3.32697273 40.974653,3.32697273 L31.8217299,3.32697273 Z M45.7127556,23.3624727 C48.1643111,24.9736545 49.9406786,26.7799273 51.0418581,28.7846545 C52.1598752,30.8213364 52.7205675,32.8176545 52.7205675,34.7736091 L50.0551744,34.7736091 C50.0551744,32.1281091 48.9775675,29.7130182 46.8223538,27.5283364 C44.6351487,25.3587909 42.0118496,24.2757 38.9524564,24.2757 L31.8217299,24.2757 L31.8217299,34.7736091 L29.1563368,34.7736091 L29.1563368,0.664654545 L40.974653,0.664654545 C44.2495675,0.664654545 47.0361915,1.82342727 49.3395761,4.14097273 C51.6581145,6.44001818 52.8182256,9.2167 52.8182256,12.4693364 C52.8182256,14.6725182 52.2423795,16.7176091 51.0906872,18.6062909 C49.9726701,20.4142455 48.179465,22.0002 45.7127556,23.3624727 Z" id="Fill-3"></path>
								<path id="letterI" d="M78.5235265,23.9299182 L71.7143983,7.07305455 L64.9035863,23.9299182 L78.5235265,23.9299182 Z M85.8024239,34.7742818 L82.9400308,34.7742818 L79.6095521,26.5670091 L63.8175607,26.5670091 L60.4870821,34.7742818 L57.6246889,34.7742818 L71.7143983,-0.000672727273 L85.8024239,34.7742818 Z" id="Fill-5"></path>
								<polygon id="letterN" id="Fill-8" points="91.4214701 34.7746182 94.0868632 34.7746182 94.0868632 0.665663636 91.4214701 0.665663636"></polygon>
								<polygon id="letterU" id="Fill-10" points="130.126582 34.6010545 104.737155 7.63982727 104.737155 34.7742818 102.073445 34.7742818 102.073445 0.912554545 127.461189 27.8973273 127.461189 0.665327273 130.126582 0.665327273"></polygon>
								<path id="letterU" d="M165.921987,0.443327273 L165.921987,17.2177818 C165.921987,21.1061455 164.531201,24.4395091 161.751312,27.2161909 C158.984893,29.9777364 155.656098,31.3601909 151.763244,31.3601909 C147.855235,31.3601909 144.519705,29.9777364 141.753286,27.2161909 C138.988551,24.4395091 137.606184,21.1061455 137.606184,17.2177818 L137.606184,0.443327273 L142.926868,0.443327273 L142.926868,17.2177818 C142.926868,19.6496909 143.790637,21.7284182 145.518175,23.4556455 C147.232244,25.1677364 149.313372,26.0221 151.763244,26.0221 C154.197962,26.0221 156.272355,25.1677364 157.986423,23.4556455 C159.713962,21.7284182 160.579415,19.6496909 160.579415,17.2177818 L160.579415,0.443327273 L165.921987,0.443327273 Z" id="Fill-11"></path>
								<path id="letterP" d="M183.091968,19.5619 C185.001353,19.5619 186.624498,18.8942182 187.961404,17.5571727 C189.315148,16.2066727 189.990336,14.5769909 189.990336,12.6714909 C189.990336,10.7643091 189.315148,9.13630909 187.961404,7.78412727 C186.624498,6.44876364 185.001353,5.78108182 183.091968,5.78108182 L176.170028,5.78108182 L176.170028,19.5619 L183.091968,19.5619 Z M183.091968,0.421127273 C186.45949,0.421127273 189.343772,1.62194545 191.749866,4.02358182 C194.154276,6.41008182 195.356481,9.29271818 195.356481,12.6714909 C195.356481,16.0334455 194.154276,18.9160818 191.749866,21.3177182 C189.343772,23.7210364 186.45949,24.9218545 183.091968,24.9218545 L176.170028,24.9218545 L176.170028,30.7510364 L170.805567,30.7510364 L170.805567,0.421127273 L183.091968,0.421127273 Z" id="Fill-12"></path>
								<polygon id="underline" id="Fill-13" points="137.605342 34.8048909 176.18535 34.8048909 176.18535 32.8876182 137.605342 32.8876182"></polygon>
							</g>
						</g>
					</g>
				</g>
			</g>
			</svg>
		</div>
	</div>

	<div id="primary" class="content-area home">
		<main id="main" class="site-main">

		<!-- area learning -->
		<div class="c-home__learning js-scrollify" data-section-name="learning">

			<div class="c-home__circle"></div>

			<div class="c-home__content">
				<svg height="100%" width="100%">
				<defs>
					<linearGradient id="grad1" x1="0%" y1="0%" x2="0%" y2="100%">
						<stop offset="50%" style="stop-color:#2B0E82;stop-opacity:1" />
						<stop offset="100%" style="stop-color:#E00146;stop-opacity:1" />
					</linearGradient>
					<linearGradient id="grad2" x1="0%" y1="0%" x2="0%" y2="100%">
						<stop offset="0%" style="stop-color:#E00146;stop-opacity:1" />
						<stop offset="60%" style="stop-color:#2B0E82;stop-opacity:1" />
					</linearGradient>
					<linearGradient id="grad3" x1="0%" y1="0%" x2="0%" y2="100%">
						<stop offset="50%" style="stop-color:#2B0E82;stop-opacity:1" />
						<stop offset="100%" style="stop-color:#E00146;stop-opacity:1" />
					</linearGradient>
				</defs>
					<line id="line5" x1="70%" y1="20%" x2="50%" y2="50%" stroke="url(#grad2)"/>
					<line id="line6" x1="70%" y1="70%" x2="50%" y2="50%" stroke="url(#grad3)"/>
				</svg>

				<div class="c-content__circle" id="circle5" data-section="creativity"><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__design.svg" alt="BRAINUP CREATIVITY"></div>
				<div class="c-content__circle" id="circle6" data-section="web design"><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__code.svg" alt="BRAINUP WEB DESIGN"></div>
			</div>
			
			<div class="c-home__title">
				<div class="c-hometitle_inner">
					<h2>BRAIN<span>UP</span> THE</h2>
					<h1>KNOWLEDGE</h1>
				</div>
				<a class="c-arrowRight" href="#"><span>insegnamento</span><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__arrow-right.png" alt="BRAINUP"></a>
			</div>

			<div class="c-home__image">
				<img class="c-homeimage__inner" src="wp-content/themes/brainup.wptheme/wp-content/images/img-hp__glasses.png" alt="BRAINUP KNOWLEDGE">
			</div>
		</div>
		<!-- /area learning -->

		<!-- area web -->
		<div class="c-home__web js-scrollify" data-section-name="web-design">

			<div class="c-home__circle"></div>

			<div class="c-home__arrowUp">
				<p>scroll up</p>
				<div class="c-arrowUp">
					<img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__arrow-up.png" alt="BRAINUP">
					<span></span>
				</div>
			</div>

			<div class="c-home__content">
				<svg height="100%" width="100%">
					<line id="line3" x1="20%" y1="75%" x2="50%" y2="50%" stroke="url(#grad1)"/>
					<line id="line4" x1="70%" y1="20%" x2="50%" y2="50%" stroke="url(#grad2)"/>
				</svg>

				<div class="c-content__circle" id="circle3" data-section="learning"><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__learning.svg" alt="BRAINUP LEARNING"></div>
				<div class="c-content__circle" id="circle4" data-section="creativity"><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__design.svg" alt="BRAINUP CREATIVITY"></div>
			</div>
			
			<div class="c-home__title">
				<div class="c-hometitle_inner">
					<h2>BRAIN<span>UP</span> THE</h2>
					<h1>POPULARITY</h1>
				</div>
				<a class="c-arrowRight" href="#"><span>siti web</span><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__arrow-right.png" alt="BRAINUP"></a>
			</div>

			<div class="c-home__image">
				<img class="c-homeimage__inner" src="wp-content/themes/brainup.wptheme/wp-content/images/img-hp__mac.png" alt="BRAINUP ORIGINALITY">
			</div>
		</div>
		<!-- /area web -->

		<!-- area design -->
		<div class="c-home__design js-scrollify" data-section-name="creativity">
			<div class="c-home__circle"></div>

			<div class="c-home__arrowUp">
				<p>scroll up</p>
				<div class="c-arrowUp">
					<img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__arrow-up.png" alt="BRAINUP">
					<span></span>
				</div>
			</div>

			<div class="c-home__content">
				<svg height="100%" width="100%">
					<line id="line1" x1="20%" y1="75%" x2="50%" y2="50%" stroke="url(#grad1)"/>
					<line id="line2" x1="70%" y1="70%" x2="50%" y2="50%" stroke="url(#grad3)"/>
				</svg>

				<div class="c-content__circle" id="circle1" data-section="learning"><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__learning.svg" alt="BRAINUP LEARNING"></div>
				<div class="c-content__circle" id="circle2" data-section="web design"><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__code.svg" alt="BRAINUP WEB DESIGN"></div>
			</div>
			
			<div class="c-home__title">
				<div class="c-hometitle_inner">
					<h2>BRAIN<span>UP</span> THE</h2>
					<h1>ORIGINALITY</h1>
				</div>
				<a class="c-arrowRight" href="#"><span>progetti</span><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__arrow-right.png" alt="BRAINUP"></a>
			</div>

			<div class="c-home__image">
				<img class="c-homeimage__inner" src="wp-content/themes/brainup.wptheme/wp-content/images/img-hp__coffe.png" alt="BRAINUP ORIGINALITY">
			</div>
		</div>
		<!-- /area design -->

		<!-- nav sezioni -->
		<div class="c-home__nav">
			<a class="js-home__nav js-learning" href="javascript:;">insegnamento</a>
			<a class="js-home__nav js-web-design" href="javascript:;">sviluppo web</a>
			<a class="js-home__nav js-creativity" href="javascript:;">creatività</a>
		</div>
		<!-- /nav sezioni -->

<?php
get_footer();


