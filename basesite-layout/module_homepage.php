
		<!-- area learning -->
		<div class="c-home__learning js-scrollify" data-section-name="learning">

			<div class="c-home__circle"></div>

			<div class="c-home__content">
				<svg height="100%" width="100%">
				<defs>
					<linearGradient id="grad1" x1="0%" y1="0%" x2="0%" y2="100%">
						<stop offset="50%" style="stop-color:#2B0E82;stop-opacity:1" />
						<stop offset="100%" style="stop-color:#E00146;stop-opacity:1" />
					</linearGradient>
					<linearGradient id="grad2" x1="0%" y1="0%" x2="0%" y2="100%">
						<stop offset="0%" style="stop-color:#E00146;stop-opacity:1" />
						<stop offset="60%" style="stop-color:#2B0E82;stop-opacity:1" />
					</linearGradient>
					<linearGradient id="grad3" x1="0%" y1="0%" x2="0%" y2="100%">
						<stop offset="50%" style="stop-color:#2B0E82;stop-opacity:1" />
						<stop offset="100%" style="stop-color:#E00146;stop-opacity:1" />
					</linearGradient>
				</defs>
					<line id="line5" x1="70%" y1="20%" x2="50%" y2="50%" stroke="url(#grad2)"/>
					<line id="line6" x1="70%" y1="70%" x2="50%" y2="50%" stroke="url(#grad3)"/>
				</svg>

				<div class="c-content__circle" id="circle5" data-section="creativity"><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__design.svg" alt="BRAINUP CREATIVITY"></div>
				<div class="c-content__circle" id="circle6" data-section="web design"><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__code.svg" alt="BRAINUP WEB DESIGN"></div>
			</div>
			
			<div class="c-home__title">
				<div class="c-hometitle_inner">
					<h2>BRAIN<span>UP</span> THE</h2>
					<h1>KNOWLEDGE</h1>
				</div>
				<a class="c-arrowRight" href="#"><span>insegnamento</span><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__arrow-right.png" alt="BRAINUP"></a>
			</div>

			<div class="c-home__image">
				<img class="c-homeimage__inner" src="wp-content/themes/brainup.wptheme/wp-content/images/img-hp__glasses.png" alt="BRAINUP KNOWLEDGE">
			</div>
		</div>
		<!-- /area learning -->

		<!-- area web -->
		<div class="c-home__web js-scrollify" data-section-name="web-design">

			<div class="c-home__circle"></div>

			<div class="c-home__arrowUp">
				<p>scroll up</p>
				<div class="c-arrowUp">
					<img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__arrow-up.png" alt="BRAINUP">
					<span></span>
				</div>
			</div>

			<div class="c-home__content">
				<svg height="100%" width="100%">
					<line id="line3" x1="20%" y1="75%" x2="50%" y2="50%" stroke="url(#grad1)"/>
					<line id="line4" x1="70%" y1="20%" x2="50%" y2="50%" stroke="url(#grad2)"/>
				</svg>

				<div class="c-content__circle" id="circle3" data-section="learning"><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__learning.svg" alt="BRAINUP LEARNING"></div>
				<div class="c-content__circle" id="circle4" data-section="creativity"><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__design.svg" alt="BRAINUP CREATIVITY"></div>
			</div>
			
			<div class="c-home__title">
				<div class="c-hometitle_inner">
					<h2>BRAIN<span>UP</span> THE</h2>
					<h1>POPULARITY</h1>
				</div>
				<a class="c-arrowRight" href="#"><span>siti web</span><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__arrow-right.png" alt="BRAINUP"></a>
			</div>

			<div class="c-home__image">
				<img class="c-homeimage__inner" src="wp-content/themes/brainup.wptheme/wp-content/images/img-hp__mac.png" alt="BRAINUP ORIGINALITY">
			</div>
		</div>
		<!-- /area web -->

		<!-- area design -->
		<div class="c-home__design js-scrollify" data-section-name="creativity">
			<div class="c-home__circle"></div>

			<div class="c-home__arrowUp">
				<p>scroll up</p>
				<div class="c-arrowUp">
					<img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__arrow-up.png" alt="BRAINUP">
					<span></span>
				</div>
			</div>

			<div class="c-home__content">
				<svg height="100%" width="100%">
					<line id="line1" x1="20%" y1="75%" x2="50%" y2="50%" stroke="url(#grad1)"/>
					<line id="line2" x1="70%" y1="70%" x2="50%" y2="50%" stroke="url(#grad3)"/>
				</svg>

				<div class="c-content__circle" id="circle1" data-section="learning"><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__learning.svg" alt="BRAINUP LEARNING"></div>
				<div class="c-content__circle" id="circle2" data-section="web design"><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__code.svg" alt="BRAINUP WEB DESIGN"></div>
			</div>
			
			<div class="c-home__title">
				<div class="c-hometitle_inner">
					<h2>BRAIN<span>UP</span> THE</h2>
					<h1>ORIGINALITY</h1>
				</div>
				<a class="c-arrowRight" href="#"><span>progetti</span><img src="wp-content/themes/brainup.wptheme/wp-content/images/icon__arrow-right.png" alt="BRAINUP"></a>
			</div>

			<div class="c-home__image">
				<img class="c-homeimage__inner" src="wp-content/themes/brainup.wptheme/wp-content/images/img-hp__coffe.png" alt="BRAINUP ORIGINALITY">
			</div>
		</div>
		<!-- /area design -->

		<!-- nav sezioni -->
		<div class="c-home__nav">
			<a class="js-home__nav js-learning" href="javascript:;">insegnamento</a>
			<a class="js-home__nav js-web-design" href="javascript:;">sviluppo web</a>
			<a class="js-home__nav js-creativity" href="javascript:;">creatività</a>
		</div>
		<!-- /nav sezioni -->
