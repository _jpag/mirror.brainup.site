<?php /* Template Name: BaseSite Default  */ get_header('page'); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		
		<section class="c-section l-margin_l">
			<div class="o-verticalText"><?php the_title(); ?></div>
				<div class="l-container">
					<?php the_field('text'); ?>
				</div>	
			</div>
		</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();