<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BaseSite
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class('page-brainup'); ?>>
<div id="cursor"></div>

<div id="page" class="site">
	<header id="masthead" class="site-header">
		<?php get_template_part('./template-parts/basesite', 'navigation'); ?>	
		<div class="site-branding js-hoverImage">
			<?php the_custom_logo(); ?>
		</div><!-- .site-branding -->
	</header><!-- #masthead -->
	<div class="header-background"></div>

	<div id="content" class="site-content">
